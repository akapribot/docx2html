# Docx2Html

Docx2Html is a Word (.docx) to HTML converter. It creates clean HTML from Docx files.


### Features
  - Include **<img>** tags for **[pic-N]** words
  - Place **<img>** tags in **<figure>** tags
  - Rename images in **image** folder to lowercase


### Version
0.1


### Tech

Docx2Html uses a number of open source projects to work properly:

* [Pandoc][2] - a universal document converter
* [jsoup][3] - Java HTML Parser
* [Flatter][4] - flat modern JavaFX theme


### Installation

 - Extract the provided zip
 - Open Docx2Html.jar


### License

MIT


### Developer

Ashutosh Kumar Singh

[aksingh.net][1]


[1]: http://aksingh.net/
[2]: http://pandoc.org/
[3]: http://jsoup.org/
[4]: http://www.guigarage.com/javafx-themes/flatter/