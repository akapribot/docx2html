package net.aksingh.docx2html;

import com.guigarage.flatterfx.FlatterFX;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import net.aksingh.docx2html.util.AppConstants;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("view_main.fxml"));
        primaryStage.setTitle(AppConstants.APP_NAME + " " + AppConstants.APP_VER);
        primaryStage.setScene(new Scene(root, 600, 450));
        primaryStage.show();

        FlatterFX.style();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
