package net.aksingh.docx2html.util;

public interface AppConstants {

    public String APP_NAME = "Docx2Html";
    public String APP_VER = "0.1";

    public String DEV_NAME = "Ashutosh Kumar Singh";
}
