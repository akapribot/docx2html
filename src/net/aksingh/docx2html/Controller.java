package net.aksingh.docx2html;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.awt.*;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Controller {

    private Stage myStage;
    private File fileSelected = new File("");
    private Document document = null;

    @FXML private Button btnAbout;
    @FXML private Button btnFile;
    @FXML private Button btnConvert;

    @FXML private CheckBox checkRemoveIds;
    @FXML private CheckBox checkFigure;
    @FXML private CheckBox checkImage;
    @FXML private CheckBox checkImageLowercase;

    @FXML private Label labelFile;
    @FXML private Label labelConvert;

    @FXML private TextField textFigureClass;

    public void setStage(Stage stage) {
        myStage = stage;
    }

    @FXML
    private void handleButtonAction(ActionEvent event) {
        if (event.getSource() == btnFile) {
            chooseFile();
        } else if (event.getSource() == btnConvert) {
            convertFile();
        } else if (event.getSource() == btnAbout) {
            openAKSinghnet();
        }
    }

    private void chooseFile() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Locate the MS Word (.docx) file");

        File file = fileChooser.showOpenDialog(myStage);
        if (file != null) {
            fileSelected = file;
        }
        labelFile.setText(fileSelected.getAbsolutePath());
    }

    private void convertFile() {
        labelConvert.setText("Converting...");

        try {
            Process process = Runtime.getRuntime().exec(".\\lib\\pandoc.exe \"" + fileSelected.getAbsolutePath()
                    + "\" --from=docx --to=html5 --output=\"" + getFilenameWithoutExtension() + ".tmp\" --smart", null, null);
            process.waitFor();

            if (process.exitValue() == 0) {
                labelConvert.setText("Parsing...");

                performOptions();
            } else {
                labelConvert.setText("Error in converting! Try again.");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void performOptions() {
        if (document == null) {
            try {
                document = Jsoup.parse(new File(getFilenameWithoutExtension() + ".tmp"), "UTF-8", "");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (checkRemoveIds.isSelected()) {
            removeIdsFromTags();
        }

        if (checkImageLowercase.isSelected()) {
            renameImageToLowercase();
        }

        if (checkImage.isSelected()) {
            putImage();
        }

        writeDocumentToFile();

        labelConvert.setText("Done!");
        document = null;
    }

    private String getFilenameWithoutExtension() {
        StringBuilder builder = new StringBuilder(fileSelected.getAbsolutePath());

        builder.delete(builder.lastIndexOf("."), builder.length());

        return builder.toString();
    }

    private void removeIdsFromTags() {
        if (document != null) {
            document.select("*").removeAttr("id");
        }
    }

    private void renameImageToLowercase() {
        File folder = new File(fileSelected.getParent() + "\\image");

        if (folder.exists() && folder.isDirectory()) {
            File[] listOfFiles = folder.listFiles();

            for (File file: listOfFiles) {
                Path path = Paths.get(file.getAbsolutePath());
                try {
                    Files.move(path, path.resolveSibling(file.getName().toLowerCase().replace(' ', '-')));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void putImage() {
        StringBuilder builder = new StringBuilder(document.body().html());
        File folder = new File(fileSelected.getParent() + "\\image");

        if (folder.exists() && folder.isDirectory()) {
            File[] listOfFiles = folder.listFiles();

            while (builder.indexOf("[pic") != -1) {
                int startInd = builder.indexOf("[pic-");
                int endInd = builder.indexOf("]", startInd);

                String no = builder.substring(startInd+5, endInd);
                String name = null;
                for (File file: listOfFiles) {
                    if (file.getName().startsWith(no)) {
                        name = file.getName();
                        break;
                    }
                }

                if (name != null) {
                    StringBuilder newTag = new StringBuilder("");
                    if (checkFigure.isSelected()) {
                        newTag.append("<figure class=\"" + textFigureClass.getText() + "\">");
                    }
                    newTag.append("<img src=\"" + name + "\">");
                    if (checkFigure.isSelected()) {
                        newTag.append("</figure>");
                    }

                    builder.replace(startInd, endInd+1, newTag.toString());
                }
            }
        }

        document = Jsoup.parse(builder.toString());
        if (document != null) {
            for (Element element : document.select("p")) {

                if (!element.hasText() && element.isBlock()) {
                    element.remove();
                }
            }

            document.select("img").attr("width", "100%");
            document.select("img").attr("height", "auto");
        }
    }

    private void writeDocumentToFile() {
        if (document != null) {
            new File(getFilenameWithoutExtension() + ".tmp").delete();

            Writer writer = null;
            try {
                writer = new BufferedWriter(new OutputStreamWriter(
                        new FileOutputStream(getFilenameWithoutExtension() + ".html"), "utf-8"));
                writer.write(document.body().html());
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            labelConvert.setText("Error in parsing! Try again.");
        }
    }

    private void openAKSinghnet() {
        URI uri = null;
        try {
            uri = new URI("http://www.aksingh.net/");

            Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
            if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
                desktop.browse(uri);
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
